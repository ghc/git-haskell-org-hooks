git-haskell-org-hooks
=====================

Binaries used by `lint` Docker image in ghc/ci-images>.

When updating this you should also bump the commit in the `lint`
image's [Dockerfile](https://gitlab.haskell.org/ghc/ci-images/blob/master/linters/Dockerfile#L29).